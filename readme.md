# AFPAS LA

AFPAS LA est un site internet permettant a un stagiare de faire une demande d'absence, et le formateur accepte ou refuse la demande.

## Environnement de développement 

### Pré-requis

* PHP 7.4 
* Composer
* Symfony CLI 
* Docker
* Docker-compose

Vous pouvez vérifier les pré-requis (Sauf Docker et Docker compose) avec la commande suivante (de la CLI Symfony) : 

```bash
Symfony check:requirements
``` 

### Lancer l'environnement de Développement 

```bash
docker-compose up -d
symfony serve -d
```
